Source: ssldump
Section: net
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libjson-c-dev,
	       libnet1-dev,
               libpcap-dev (>= 0.6.2),
               libssl-dev,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/adulau/ssldump
Vcs-Git: https://salsa.debian.org/pkg-security-team/ssldump.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/ssldump

Package: ssldump
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: tcpdump
Description: SSLv3/TLS network protocol analyzer
 This program will dump the traffic on a network and analyze it for
 SSLv3/TLS network traffic, typically used to secure TCP connections.
 When it identifies this traffic, it decodes the results.  When
 provided with the appropriate keying material, it will also decrypt
 the connections and display the application data traffic.
 .
 ssldump is based on tcpdump, a network monitoring and data acquisition
 tool.
